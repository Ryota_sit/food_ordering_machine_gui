import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachineGUI {
    private JPanel root;
    private JButton beefBowlButton;
    private JButton beefBowlWithToppingsButton;
    private JButton porkBowlButton;
    private JButton friedChickenBowlButton;
    private JButton porkMisoSoupButton;
    private JButton draftBeerButton;
    private JRadioButton smallRadioButton;
    private JRadioButton mediumRadioButton;
    private JRadioButton largeRadioButton;
    private JLabel selectSizeLabel;
    private JSpinner spinner1;
    private JLabel selectQuantityLabel;
    private JButton checkOutButton;
    private JTextArea Ordereditems;
    private JLabel totalPriceLabel;
    private JLabel orderedItemsLabel;
    private JLabel totalLabel;
    private JLabel beefBowlLabel;
    private JLabel beefBowlWithGreenLabel;
    private JLabel porkBowlLabel;
    private JLabel a550yenLabel;
    private JLabel a400yenLabel;
    private JLabel a380yenLabel;
    private JLabel friedChickenBowlLabel;
    private JLabel a580yenLabel;
    private JLabel porkMisoSoupLabel;
    private JLabel a200yenLabel;
    private JLabel draftBeerLabel;
    private JLabel a230yenLabel;
    private JLabel titleLabel;

    int totalPrice = 0;
    int quantity = 1;

    // ～～～ ↓ メソッドの宣言 ～～～
    void order(String food, String size, int quantity, int price){
        if (quantity > 0){      // 数量が０より大きい場合の処理
            int confirmation = JOptionPane.showConfirmDialog(
                    null,
                    "Would you like to confirm your order with the following information?\n"
                            + "If you want to change the size or quantity, please cancel your order once.\n\n"
                            + "Menu : " + food + "\n"
                            + "Size : " + size + "\n"
                            + "Quantity : " + quantity + "\n"
                            + "Unit Price : " + price + "\n"
                            + "Total Price : " + price * quantity + "\n\n",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION
            );
            if (confirmation == 0) {    // お客さんが注文を確定したときのダイアログボックスの処理
                String currentText = Ordereditems.getText();    // 注文リストの現在のテキストの保持
                Ordereditems.setText(currentText + food + " (" + size + ") ×" + quantity + " → " + price*quantity +  " yen\n\n");  // 注文リストに新しい注文を追加
                JOptionPane.showMessageDialog(
                        null,
                        "Order for\n" + "   " + food + " (" + size + ") ×" + quantity + "\n" + "received."
                );
                totalPrice += price * quantity; //  合計金額の算出
                totalPriceLabel.setText(totalPrice + " yen");   // 合計金額の表示
            }
        }
        else {      // 数量が0以下だった場合の処理
            JOptionPane.showMessageDialog(
                    null,
                    "Quantity is less than or equal to 0. Please re-select the quantity."
            );
        }
    }
    // ～～～ ↑ メソッドの宣言 ～～～

    public FoodOrderingMachineGUI() {

        // ～～～ ↓ 各メソッドの呼び出し ～～～
        beefBowlButton.setIcon(new ImageIcon(   // ボタンの写真の読み込み
                this.getClass().getResource("gyu-don.jpg")
        ));
        beefBowlButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (smallRadioButton.isSelected())          // sizeがsmallだった時の処
                    // smallRadioButton.isSelected()でラジオボタンが選択されているかを取得できる
                    order("Beef Bowl", "small", quantity, 380);
                else if (mediumRadioButton.isSelected())    // sizeがmediumだった時の処理
                    order("Beef Bowl", "medium", quantity, 400);
                else if (largeRadioButton.isSelected())     // sizeがlargeだった時の処理
                    order("Beef Bowl", "large", quantity, 580);
            }
        });

        beefBowlWithToppingsButton.setIcon(new ImageIcon(
                this.getClass().getResource("negitama-gyu-don.jpg")
        ));
        beefBowlWithToppingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (smallRadioButton.isSelected())
                    order("Beef Bowl with Green Onions and Egg", "small", quantity, 530);
                else if (mediumRadioButton.isSelected())
                    order("Beef Bowl with Green Onions and Egg", "medium", quantity, 550);
                else if (largeRadioButton.isSelected())
                    order("Beef Bowl with Green Onions and Egg", "large", quantity, 730);
            }
        });

        porkBowlButton.setIcon(new ImageIcon(
                this.getClass().getResource("buta-don.jpg")
        ));
        porkBowlButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (smallRadioButton.isSelected())
                    order("Pork Bowl", "small", quantity, 360);
                else if (mediumRadioButton.isSelected())
                    order("Pork Bowl", "medium", quantity, 380);
                else if (largeRadioButton.isSelected())
                    order("Pork Bowl", "large", quantity, 560);
            }
        });

        friedChickenBowlButton.setIcon(new ImageIcon(
                this.getClass().getResource("karaage-don.jpg")
        ));
        friedChickenBowlButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (smallRadioButton.isSelected())
                    order("Fried Chicken Bowl", "small", quantity, 560);
                else if (mediumRadioButton.isSelected())
                    order("Fried Chicken Bowl", "medium", quantity, 580);
                else if (largeRadioButton.isSelected())
                    order("Fried Chicken Bowl", "large", quantity, 760);
            }
        });

        porkMisoSoupButton.setIcon(new ImageIcon(
                this.getClass().getResource("ton-jiru.jpg")
        ));
        porkMisoSoupButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (smallRadioButton.isSelected())
                    order("Pork Miso Soup", "small", quantity, 180);
                else if (mediumRadioButton.isSelected())
                    order("Pork Miso Soup", "medium", quantity, 200);
                else if (largeRadioButton.isSelected())
                    order("Pork Miso Soup", "large", quantity, 380);
            }
        });

        draftBeerButton.setIcon(new ImageIcon(
                this.getClass().getResource("beer.jpg")
        ));
        draftBeerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {    // 年齢確認のための処理
                int ageVerification = JOptionPane.showConfirmDialog(    // 年齢確認のためのダイアログボックスの処理
                        null,
                        "This item is alcoholic beverage. Are you over 20 years old?",
                        "Age Verification",
                        JOptionPane.YES_NO_OPTION
                );
                if(ageVerification == 0){
                    if (smallRadioButton.isSelected())
                        order("Draft Beer", "small", quantity, 210);
                    else if (mediumRadioButton.isSelected())
                        order("Draft Beer", "medium", quantity, 230);
                    else if (largeRadioButton.isSelected())
                        order("Draft Beer", "large", quantity, 410);
                }
            }
        });
        // ～～～ ↑ 各メソッドの呼び出し ～～～

        // ～～～ ↓ チェックアウト時の処理 ～～～
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(   // チェックアウトするかどうか確認のための処理
                        null,
                        "Would you like to Check Out?\n",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation == 0){  // チェックアウトする場合の処理
                    JOptionPane.showMessageDialog(
                            null,
                            "Thank you. The total price is " + totalPrice + " yen.\n"
                    );
                    Ordereditems.setText("");   // 注文リストの初期化
                    totalPrice = 0;     // 合計金額の初期化
                    totalPriceLabel.setText("0 yen");   // 初期化された後の合計金額の表示
                }
            }
        });
        // ～～～ ↑ チェックアウト時の処理 ～～～

        // ～～～ ↓ 数量選択(spinner)に関する処理 ～～～
        SpinnerModel spinnerModel = new SpinnerNumberModel();   // SpinnerModelの作成
        JSpinner jSpinner = new JSpinner(spinnerModel);     // JSpinnerの作成
        JSpinner.DefaultEditor spinnerEditor = (JSpinner.DefaultEditor) spinner1.getEditor();   // デフォルト表示のカスタマイズ
        spinnerEditor.getTextField().setText("1");  // 表示値を1に変更

        spinner1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                quantity = (int) spinner1.getValue();   // Spinnerの表示値の取得
            }
        });
        // ～～～ ↑ 数量選択(spinner)に関する処理 ～～～
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachineGUI");
        frame.setContentPane(new FoodOrderingMachineGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        UIManager.put("OptionPane.messageFont", new Font("BIZ UDGothic", Font.PLAIN, 14));  // ダイアログボックスのフォント, サイズの定義

    }
}
